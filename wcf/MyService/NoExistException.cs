﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MyService
{
    [Serializable]
    public class NoExistException
    {
        [DataMember]
        public string message;
        public NoExistException(string message)
        {
            this.message = message;
        }
    }
}
