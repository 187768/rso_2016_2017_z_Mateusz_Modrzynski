#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <iostream>

#define _LITTLE_ENDIAN 0
#define _BIG_ENDIAN 1

using namespace std;

int getEndianess(){
  int num = 1;
  if(*(char *)&num == 1){
    return _LITTLE_ENDIAN;
  }
  else{
    return _BIG_ENDIAN;
  }
}

void changeByteOrder(int *data){
  int conv = 0;
  char *olddata = (char *)data;
  char *newdata = (char *)&conv;
  newdata[0] = olddata[3];
  newdata[1] = olddata[2];
  newdata[2] = olddata[1];
  newdata[3] = olddata[0];
  *data = conv;
  return;
}

int getPort(int p){
  int port = p;
  if(getEndianess() == _BIG_ENDIAN){
    changeByteOrder(&port);
  }
  return port;
}

int main(){
  int sockfd;
  struct sockaddr_in address;
  char request_header[5];
  char response_header[5];
  int endianess = getEndianess(); 
  int result;
  
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = inet_addr ("127.0.0.1");
  address.sin_port = getPort(9734);
  
  result = connect(sockfd, (struct sockaddr *) &address, sizeof (address));
  
  if (result == -1){
    perror ("client");
    exit(1);
  }
  
  cout << endl;
  cout << "p <liczba>\tWyświetla pierwiastek podanej liczby" << endl;
  cout << "t\t\tWyświetla bieżący czas na serwerze" << endl;
  cout << "exit\t\tKończy działanie programu" << endl << endl;
  string command;
  
  while(1){  
    cout << "zapytanie> ";
    getline(cin, command);
    
    if(command == "exit"){
      break;
    }
    else if(command == "t"){
      strcpy(request_header, "0002");
      write(sockfd, &request_header, sizeof(request_header));
    }
    else if(command.find("p ") == 0){  
      double number = atof(command.substr(2,command.length()).c_str());
      
      if(number <= 0){
	       cout << "# Podaj liczbę większą od 0" << endl;
	       continue;
      }
      else{
	       strcpy(request_header, "0001");
	       write(sockfd, &request_header, sizeof(request_header));
	       write(sockfd, &number, sizeof(number));
      }
    }
    else{
      cout << "# Polecenie '" + command + "' nie zostało rozpoznane" << endl;
      continue;
    }
    
    read(sockfd, &response_header, sizeof(response_header));
    
    if(strcmp(response_header, "1001") == 0){
      double response;
      read(sockfd, &response, sizeof(response));
      cout << "odpowiedź: " << response << endl;
    }
    else if(strcmp(response_header, "1002") == 0){
      int size;
      read(sockfd, &size, sizeof(size));
      
      if(endianess == _LITTLE_ENDIAN){
	changeByteOrder(&size);
      }
      
      char *response = new char[size]();
      read(sockfd, response, size);
      cout << "odpowiedź: " << response << endl;
    }
    
  }
  
  close (sockfd); 
  exit(0);
  
}