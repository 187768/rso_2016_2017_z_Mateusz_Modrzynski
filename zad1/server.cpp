#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <signal.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <math.h>
#include <string>
#include <string.h>
#include <iostream>
#include <time.h>
#include <stdlib.h>

#define _LITTLE_ENDIAN 0
#define _BIG_ENDIAN 1

using namespace std;

int getEndianess(){
  int num = 1;
  if(*(char *)&num == 1){
    return _LITTLE_ENDIAN;
  }
  else{
    return _BIG_ENDIAN;
  }
}

void changeByteOrder(int *data){
  int conv = 0;
  char *olddata = (char *)data;
  char *newdata = (char *)&conv;
  newdata[0] = olddata[3];
  newdata[1] = olddata[2];
  newdata[2] = olddata[1];
  newdata[3] = olddata[0];
  *data = conv;
  return;
}

int getPort(int p){
  int port = p;
  if(getEndianess() == _BIG_ENDIAN){
    changeByteOrder(&port);
  }
  return port;
}

int main(){
  int server_sockfd, client_sockfd;
  int endianess = getEndianess();
  socklen_t client_len;
  struct sockaddr_in server_address;
  struct sockaddr_in client_address;
   
  server_sockfd = socket (AF_INET, SOCK_STREAM, 0);
  
  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr = inet_addr("127.0.0.1");
  server_address.sin_port = getPort(9734);

  bind (server_sockfd, (struct sockaddr *) &server_address, sizeof (server_address));
  
  listen (server_sockfd, 5);
   
  while (1){      
    client_len = sizeof (client_address);
    client_sockfd = accept(server_sockfd, (struct sockaddr *) &client_address, &client_len);

    pid_t pid = fork();
    
    if(pid == 0){
      char request_header[5];
      while(read(client_sockfd, &request_header, sizeof(request_header)) > 0){
	
	if(strcmp(request_header,"0001") == 0){
	  double number;
	  double result;
	  read(client_sockfd, &number, sizeof(number));
	  result = sqrt(number);
      
	  request_header[0] = '1';     
	  write(client_sockfd, &request_header, sizeof(request_header));
	  write(client_sockfd, &result, sizeof(result));
	}
	else if(strcmp(request_header,"0002") == 0){
	  int length;
	  char *time_str;
	  time_t servertime;
	  time(&servertime);
	  time_str = ctime(&servertime);
	  length = strlen(time_str);
	  
	  if(endianess == _LITTLE_ENDIAN){
	    changeByteOrder(&length);
	  }
      
	  request_header[0] = '1';
	  
	  write(client_sockfd, &request_header, sizeof(request_header));  
	  write(client_sockfd, &length, sizeof(length));
	  write(client_sockfd, ctime(&servertime), 24);
	}
      }
      _Exit(0);
    }
    else{
      close(client_sockfd);
    }   
  }
  
}